﻿using System;
using System.Threading.Tasks;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Connector;

namespace azbotapp.Controllers
{
    [Serializable]
    public class SimpleDialog : IDialog<object>
    {
        public Task StartAsync(IDialogContext context)
        {
            context.Wait(MessageReceivedAsync);

            return Task.CompletedTask;
        }

        private async Task MessageReceivedAsync(IDialogContext context, IAwaitable<object> result)
        {
            var activity = await result as IMessageActivity;
            await context.PostAsync("翰林客服您好，請輸入服務代碼(1~3):");
            await context.PostAsync("<1>訂書 <2>查詢訂單 <3>線上客服");
            context.Wait(MessageReceivedOperationChoice);
        }

        private async Task MessageReceivedOperationChoice(IDialogContext context, IAwaitable<IMessageActivity> result)
        {
            var message = await result;
            if (message.Text.ToLower().Equals("1", StringComparison.InvariantCultureIgnoreCase))
            {
                await context.PostAsync("請輸入書名");
                context.Wait(MessageReceivedOrder);
            }
            else if (message.Text.ToLower().Equals("2", StringComparison.InvariantCultureIgnoreCase))
            {
                await context.PostAsync("請輸入單號");
                context.Wait(MessageReceivedQuery);
            }
            else if (message.Text.ToLower().Equals("3", StringComparison.InvariantCultureIgnoreCase))
            {
                await context.PostAsync("請輸入您的問題");
                context.Wait(MessageReceivedCustom);
            }
            else
            {
                await context.PostAsync("輸入錯誤，請輸入正確服務代碼");
                await context.PostAsync("< 1 > 訂書 < 2 > 查詢訂單 < 3 > 線上客服");
                context.Wait(MessageReceivedAsync);//回原點

            }
        }

        //訂書
        private async Task MessageReceivedOrder(IDialogContext context, IAwaitable<IMessageActivity> result)
        {
            var message = await result;
            await context.PostAsync($"您輸入的書名是:{message.Text}");
            await context.PostAsync($"確定下單嗎?(y/n)");
            context.Wait(MessageReceivedOrderSure);
        }

        //確認下單
        private async Task MessageReceivedOrderSure(IDialogContext context, IAwaitable<IMessageActivity> result)
        {
            var message = await result;
            if (message.Text.ToLower() == "y")
            {
                await context.PostAsync($"訂單成立感謝您!!");
            }
            else
            {
                await context.PostAsync($"訂單取消");
                
            }
            context.Wait(MessageReceivedAsync);
        }

        //查單
        private async Task MessageReceivedQuery(IDialogContext context, IAwaitable<object> result)
        {
            await context.PostAsync("查詢訂單結束");
            context.Wait(MessageReceivedAsync);
        }

        //客服
        private async Task MessageReceivedCustom(IDialogContext context, IAwaitable<object> result)
        {
            await context.PostAsync("線上客服結束");
            context.Wait(MessageReceivedAsync);
        }


    }
}